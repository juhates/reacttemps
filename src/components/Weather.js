import React from "react";

//IF WE RETURN ONLY ONE SINGLE ARGUMENT WE CAN DO THIS :::
const Weather = props => (
    <div class="weather-data">
        {props.city && props.country && <p>Locaiton { props.city } { props.country }</p> }
        {props.temperature && <p>Temperature { props.temperature }</p> }
        {props.humidity && <p>Humidity { props.humidity }</p> }
        {props.description && <p>Conditions { props.description }</p> }
        {props.error && <p> { props.error }</p> }
    </div>
)

//IF WE RETURN ONLY ONE ELEMENT WE CAN DO THIS :::
/*
const Weather = (props) => (
    <div>
        {props.city && props.country && <p>Locaiton { props.city } { props.country }</p> }
        {props.temperature && <p>temperature { props.temperature }</p> }
        {props.humidity && <p>humidity { props.humidity }</p> }
        {props.description && <p>Conditions { props.description }</p> }
        {props.error && <p> { props.error }</p> }
    </div>
)
*/

/*
const Weather = (props) =>{
    return(
        <div>
            {props.city && props.country && <p>Locaiton { props.city } { props.country }</p> }
            {props.temperature && <p>temperature { props.temperature }</p> }
            {props.humidity && <p>humidity { props.humidity }</p> }
            {props.description && <p>Conditions { props.description }</p> }
            {props.error && <p> { props.error }</p> }
        </div>
    )
}
*/

//CLASS EXAMPLE
/*
class Weather extends React.Component{
    render(){
        return (
            <div>
                {this.props.city && this.props.country && <p>Locaiton { this.props.city } { this.props.country }</p> }
                {this.props.temperature && <p>temperature { this.props.temperature }</p> }
                {this.props.humidity && <p>humidity { this.props.humidity }</p> }
                {this.props.description && <p>Conditions { this.props.description }</p> }
                {this.props.error && <p> { this.props.error }</p> }
            </div>
        );
    }
}*/
export default Weather;