import React from "react";

const Form = props => (
    <form class="weather-form" onSubmit={props.getWeather}>
        <p>
            <span class="input_gradient">
                <input type="text" name="city" placeholder="city..."></input>
                <span></span>	
            </span>
            <span class="input_gradient">
                <input type="text" name="country" placeholder="country"></input>
                <span></span>	
            </span>
        </p>
        <button class="btn">Get weather</button>
    </form>
);

export default Form;