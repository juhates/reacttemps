
import React from "react"
import testgif from '../assets/test.gif'
import background from '../assets/background.gif'

const Titles = () => (
    <div>
        <img src={background} alt="loading..." />
        <h1>Weather finder</h1>
        <h2>Select country and city</h2>
    </div>
)

export default Titles;
/*
class Titles extends React.Component{
    render(){
        return(
            <div>
                <img src={background} alt="loading..." />
                <h1>Weather finder</h1>
                <p>find out temperature</p>
            </div>
        );
    }
}
*/