var mqtt = require('mqtt')
var client  = mqtt.connect('mqtt://test.mosquitto.org')

client.on('connect', function () {
    client.subscribe('makkara/test', function (err) {
        if (!err) {
        client.publish('makkara/test', 'Hello mqtt')
        }
    })
})

client.on('message', function (topic, message) {
    // message is Buffer
    console.log(topic.toString(), message.toString())
    client.end()  
})



/*
var mqtt = require('mqtt')
var client  = mqtt.connect('mqtt://test.mosquitto.org')

const express = require('express')
const app = express()
const port = 3000
const path = require('path')
const mqtt = require('mqtt')
const server = require("http").createServer(app)
const io = require("socket.io")(server)
const client = mqtt.connect("mqtt://iot.eclipse.org")


app.use(express.static('public'))

var queue = []
let started = false
let ready = false
let running = false
let sendSocket = null

app.get('/', function(req, res){
    if(!ready){
        client.publish("DHT11_IO_FI/C2", "READY")
    }
    res.sendFile(path.join(__dirname + '/templates/index.html'));
});
server.listen(port)






*/