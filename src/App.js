import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Titles from "./components/Titles";
import Form from './components/Form';
import Weather from './components/Weather';

const API_KEY = "5d1ee3f9410b54fc5f53b500c97e6635";  // API_key
// juha makkarakotka6
// mongodb://<dbuser>:<dbpassword>@ds161653.mlab.com:61653/node_weather_db

class App extends Component {
  state = { 
    temperature:undefined,
    city:undefined,
    country:undefined,
    humidity:undefined,
    description:undefined,
    error:undefined
  }
  getWeather = async(e) => {
    const city = e.target.elements.city.value;        // Selected City name
    const country = e.target.elements.country.value;  // Selected Country name
    e.preventDefault();
    const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&APPID=${API_KEY}&units=metric`); //template strings
    const data = await api_call.json();
    if(city && data){ // If we get city and we get data then we do this
      console.log(data);
      //DONT NEVER MANIPULATE STATE STRAIGHT
      this.setState({  
        temperature:data.main.temp,
        city:data.name,
        country:data.sys.country,
        humidity:data.main.humidity,
        description:data.weather[0].description, // description is inside object
        error:""
      });
    }
    else{ 
      this.setState({  
        temperature:undefined,
        city:undefined,
        country:undefined,
        humidity:undefined,
        description:undefined,
        error:"Sir... Please insert some city and country value...!"
      });
      console.log("State is undefined: \n",this.state);
    }
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <Titles />
          <Form getWeather={this.getWeather}/>
          <Weather
            temperature={this.state.temperature}
            city={this.state.city}
            country={this.state.country}
            humidity={this.state.humidity}
            description={this.state.description}
            error={this.state.error}           
            />
        </header>
      </div>
    );
  }
}

export default App;